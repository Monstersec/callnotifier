import React, {Component} from 'react';
import {
  View,
  Text,
  PermissionsAndroid,
  Button,
  Dimensions,
  ToastAndroid,
  TouchableOpacity,
} from 'react-native';
import Modal from 'react-native-modal';
import Contacts from 'react-native-contacts';
import AsyncStorage from '@react-native-community/async-storage';
import {RecyclerListView, DataProvider, LayoutProvider} from 'recyclerlistview';
const SCREEN_WIDTH = Dimensions.get('window').width;
var fakeData = [];
import {Card, CardItem, Thumbnail} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {CheckBox} from 'react-native-elements';
import CallDetectorManager from 'react-native-call-detection';
import {SearchBar} from 'react-native-elements';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      call_status: 'Call status',
      contact_list_search: [],
      contact_list_search_2: [],
      contact_list: new DataProvider((r1, r2) => r1 == r2).cloneWithRows(
        fakeData,
      ),
      valid_caller: true,
      contact_list_for_updation: [],
      call_notify_popup: false,
      caller_name: 'Caller name',
      caller_number: 'Caller number',
    };
    this.layoutProvider = new LayoutProvider(
      i => {
        return this.state.contact_list.getDataForIndex(i).type;
      },
      (type, dim) => {
        switch (type) {
          case 'NORMAL':
            dim.width = SCREEN_WIDTH;
            dim.height = 75;
            break;
          default:
            dim.width = SCREEN_WIDTH;
            dim.height = 100;
            break;
        }
      },
    );
  }

  startListenerTapped() {
    console.log('startListenerTapped INSIDE');
    this.callDetector = new CallDetectorManager(
      (event, phoneNumber) => {
        if (event === 'Disconnected') {
          if (this.state.valid_caller) {
            this.setState({
              call_status: 'Disconnected call',
              valid_caller: false,
            });
          }
        } else if (event === 'Connected') {
          console.log('startListenerTapped   Connected ', phoneNumber);
        } else if (event === 'Incoming') {
          console.log('startListenerTapped  Incoming  ', phoneNumber);

          AsyncStorage.getItem('stored_contacts_list').then(
            stored_contacts_list => {
              var stored_list = JSON.parse(stored_contacts_list);
              console.log('store_contacts list ###  ', stored_list);
              try {
                if (stored_list.length > 0) {
                  for (let i = 0; i < stored_list.length; i++) {
                    console.log('stored_list   ###', stored_list[i].number);
                    if (stored_list[i].number == phoneNumber) {
                      this.setState({
                        call_status: 'Incoming call',
                        valid_caller: true,
                        call_notify_popup: true,
                        caller_number: phoneNumber,
                        caller_name: stored_list[i].name,
                      });
                    }
                    break;
                  }
                }
              } catch (e) {
                console.log('no users');
              }
            },
          );
        } else if (event === 'Missed') {
          console.log('startListenerTapped    Missed', phoneNumber);

          if (this.state.valid_caller) {
            this.setState({
              call_status: 'Missed call',
              valid_caller: false,
            });
          }
        }
      },
      true,
      () => {},
      {
        title: 'Phone State Permission',
        message:
          'This app needs access to your phone state in order to react and/or to adapt to incoming calls.',
      },
    );
  }

  async componentDidMount() {
    this.startListenerTapped();
    //this.GetContacts();

    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE,
        {
          title: 'Read Phone state',
          message: 'We need your permission to Read Phone state',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Thank you for your permission! :)');
      }
    } catch (err) {
      console.warn(err);
    }
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_CALL_LOG,
        {
          title: 'Read Call log',
          message: 'We need your permission to Read Call log',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Thank you for your permission! :)');
      }
    } catch (err) {
      console.warn(err);
    }
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS,
        {
          title: 'Write contacts',
          message: 'We need your permission to Write contacts',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Thank you for your permission! :)');
        //this.GetContacts();
      }
    } catch (err) {
      console.warn(err);
    }
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
        {
          title: 'Read contacts',
          message: 'We need your permission to Read contacts',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Thank you for your permission! :)');

        this.GetContacts();
      }
    } catch (err) {
      console.warn(err);
    }
  }
  componentWillUnmount() {
    this.stopListenerTapped();
  }
  stopListenerTapped() {
    this.callDetector && this.callDetector.dispose();
  }

  async GetContacts() {
    await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS,
      {
        title: 'Contacts',
        message: 'This app would like to view your contacts.',
      },
    )
      .then(() => {
        Contacts.getAll().then(contacts => {
          //console.log('Contacts.getAll()  ', contacts);
          var new_contact_list = [];
          contacts.map((value, index) => {
            //console.log('contacts individual :    ', index);

            try {
              new_contact_list.push({
                type: 'NORMAL',
                item: {
                  index: new_contact_list.length,
                  name: value.displayName,
                  number: value.phoneNumbers[0].number,
                  checked: false,
                },
              });
              this.state.contact_list_for_updation.push({
                name: value.displayName,
                number: value.phoneNumbers[0].number,
                checked: false,
              });
            } catch (e) {
              console.log('contacts individual  catch,    ' + e);
            }
          });

          this.setState({
            contact_list: new DataProvider((r1, r2) => r1 !== r2).cloneWithRows(
              new_contact_list,
            ),
          });
          // contacts returned
        });
      })
      .catch(err => {
        console.log('catch  ', err);
      });
  }

  ReturnSelectionIcon(data) {
    //   console.log('ReturnSelectionIcon  ****  ', data.item);
    if (data.item.checked) {
      var arr_x = this.state.contact_list._data;

      return (
        <CheckBox
          checked={data.item.checked}
          onPress={() => {
            try {
              arr_x[data.item.index].item.checked = !arr_x[data.item.index].item
                .checked;

              this.setState({
                contact_list: new DataProvider(
                  (r1, r2) => r1 == r2,
                ).cloneWithRows(arr_x),
              });
            } catch (err) {
              console.log('ReturnSelectionIcon  catch   ', err);
            }
          }}
        />
      );
    } else {
      var arr_x = this.state.contact_list._data;

      return (
        <CheckBox
          checked={data.item.checked}
          onPress={() => {
            try {
              arr_x[data.item.index].item.checked = !arr_x[data.item.index].item
                .checked;

              this.setState({
                contact_list: new DataProvider(
                  (r1, r2) => r1 == r2,
                ).cloneWithRows(arr_x),
              });
            } catch (err) {
              console.log('ReturnSelectionIcon  ## catch   ', err);
            }
          }}
        />
      );
    }
  }
  rowRenderer = (type, data) => {
    try {
      return (
        <Card
          style={{
            backgroundColor: 'white',
            paddingVertical: 5,
            paddingHorizontal: 5,
            marginLeft: 5,
            marginRight: 5,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <View style={{}}>
            <Text style={{fontSize: 15, opacity: 0.7, color: '#2c3e50'}}>
              {data.item.name}
            </Text>
            <Text
              style={{
                fontSize: 15,
                marginTop: 5,
                opacity: 0.8,
                fontWeight: 'bold',
                color: '#2c3e50',
              }}>
              {data.item.number}
            </Text>
          </View>
          {this.ReturnSelectionIcon(data)}
        </Card>
      );
    } catch (e) {
      console.log('renderer error:     ' + e);
    }
  };
  SaveContact() {
    var store_contacts = [];

    // AsyncStorage.setItem(
    //   'stored_contacts_list',
    //   JSON.stringify(store_contacts),
    // );

    AsyncStorage.getItem('stored_contacts_list').then(stored_contacts_list => {
      var stored_list = JSON.parse(stored_contacts_list);
      console.log('store_contacts list ###  ', stored_list);

      if (stored_list == null) {
        stored_list = [];
      }
      this.state.contact_list._data.map((item, index) => {
        console.log('loop 1');
        if (item.item.checked) {
          stored_list.push({
            number: item.item.number,
            name: item.item.name,
            index: stored_list.length,
            checked: true,
          });
        }
      });

      AsyncStorage.setItem('stored_contacts_list', JSON.stringify(stored_list));

      ToastAndroid.show('Contact added ', ToastAndroid.SHORT);
    });

    setTimeout(() => {
      this.GetSavedContact();
    }, 1000);
    console.log('store_contacts list   ', store_contacts);
  }
  GetSavedContact() {
    AsyncStorage.getItem('stored_contacts_list').then(stored_contacts_list => {
      var stored_list = JSON.parse(stored_contacts_list);
      //  console.log('store_contacts list ###  ', stored_list);

      stored_list.map((item, index) => {
        console.log('store_contacts   ***   ', item.name);
      });
      ToastAndroid.show('Contact added ', ToastAndroid.SHORT);
    });
  }
  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          paddingVertical: 15,
        }}>
        <View
          style={{
            flex: 1,
            width: '100%',
          }}>
          <RecyclerListView
            style={{flex: 1}}
            rowRenderer={this.rowRenderer}
            dataProvider={this.state.contact_list}
            layoutProvider={this.layoutProvider}
          />
        </View>
        <View
          style={{
            justifyContent: 'space-between',

            width: '100%',
            paddingHorizontal: 15,
            flexDirection: 'row',
          }}>
          <View
            style={{
              backgroundColor: 'red',
              marginVertical: 10,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Button title="Save contact" onPress={() => this.SaveContact()} />
          </View>
          <View
            style={{
              backgroundColor: 'yellow',
              marginVertical: 10,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Button title="Get contact" onPress={() => this.GetContacts()} />
          </View>
        </View>
        <Modal
          isVisible={this.state.call_notify_popup}
          animationInTiming={100}
          animationOutTiming={100}
          onBackButtonPress={() =>
            this.setState({
              call_notify_popup: !this.state.call_notify_popup,
            })
          }
          onBackdropPress={() =>
            this.setState({
              call_notify_popup: !this.state.call_notify_popup,
            })
          }
          hasBackdrop={true}
          useNativeDriver={true}
          style={{backgroundColor: 'transparent'}}>
          <Card
            style={{
              backgroundColor: 'white',
              padding: 10,
              borderRadius: 5,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                color: '#2c3e50',
                fontSize: 20,
                alignSelf: 'center',

                fontWeight: 'bold',
                opacity: 0.7,
              }}>
              {this.state.call_status.toUpperCase()}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                marginVertical: 20,
                width: '100%',
              }}>
              <Text
                style={{
                  color: '#2c3e50',
                  fontSize: 18,

                  opacity: 0.7,
                  alignSelf: 'center',
                  fontWeight: 'bold',
                }}>
                {this.state.caller_name} -
              </Text>
              <Text
                style={{
                  color: '#2c3e50',
                  fontSize: 18,

                  alignSelf: 'center',
                  fontWeight: 'bold',
                  opacity: 0.8,
                }}>
                {this.state.caller_number}
              </Text>
            </View>
            <View
              style={{
                width: '50%',
                marginVertical: 10,
                marginTop: 15,
              }}>
              <Button
                title="Close"
                onPress={() => {
                  this.setState({
                    call_notify_popup: false,
                    call_status: 'Call status',
                    caller_name: 'Caller name',
                    caller_number: 'Call number',
                  });
                }}
              />
            </View>
          </Card>
        </Modal>
      </View>
    );
  }
}

export default App;
